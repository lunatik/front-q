(function() {
    'use strict';

    //------------------------------------------------------- Cacher
    function Cacher(timeout) {
        // time in milliseconds to preserve value (0 - infinity)
        // 2 minutes by default
        var DEFAULT_CASH_TIME_LIMIT = 2 * 60 * 1000;

        this.timeout = timeout ? timeout : DEFAULT_CASH_TIME_LIMIT;
        this.storageUrl = '/cache';
    }

    Cacher.prototype.resetKey = function(key) {
        var data = this._get();

        delete data[key];

        this._set(data);
    }

    Cacher.prototype.cache = function(key, value) {
        var data = this._get();
        
        data[key] = {
            value: value,
            updatedAt: (new Date).getTime()
        };

        this._set(data);
    }

    Cacher.prototype.get = function(key) {
        var data = this._get()[key];

        if(!data) {
            return undefined;
        }

        if(this.timeout !== 0 && (new Date).getTime() - parseInt(data.updatedAt) > this.timeout) {
            this.resetKey(key);
        }

        return data.value;
    }

    Cacher.prototype._set = function(data) {
        localStorage.setItem(this.storageUrl, JSON.stringify(data));
    }

    Cacher.prototype._get = function() {
        var cache = localStorage.getItem(this.storageUrl);
        return cache ? JSON.parse(cache) : {};
    }
    //------------------------------------------------------- Cacher end

    function get(url, success_cb, error_cb) {
        var value = cacher.get(url);
        if(value) {
            return success_cb(value);
        }

        var r = new XMLHttpRequest();
        
        r.open('GET', url, true);

        r.onload = function (e) {
            if (r.readyState === 4) {
                if (r.status === 200) {
                    cacher.cache(url, r.responseText);
                    success_cb(r.responseText);
                } else {
                    error_cb();
                }
            }
        };

        r.onerror = error_cb;

        r.send();
    }

    function parseFile(url, data) {
        if(/\.json$/.test(url)) {
            return JSON.parse(data).text;
        }

        if(/\.js$/.test(url)) {
            window.cb = function(data) {
                return data;
            }
            var res = eval(data);
            delete window.cb;
            return res.text;
        }

        if(/\.txt$/.test(url)) {
            return data;
        }

        throw new TypeError('File format is not supported');
    }

    function sync() {
        output.value = 'Загрузка...';
        var url = input.value;

        var error = function(){
            output.value = 'Не удалось загрузить файл!';
        };

        var success = function(data){
            try {
                output.value = parseFile(url, data);
            } catch(e) {
                error();
            }
        };

        get(url, success, error);
    }

    var cacher = new Cacher();
    var input = document.getElementById('input');
    var output = document.getElementById('output');

    window.onload = input.onchange = sync;
})();
